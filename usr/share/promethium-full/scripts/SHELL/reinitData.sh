#!/bin/bash

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


## MongoDB Re-init

# Delete old mongo db data/files
service mongod stop
rm -fr /var/lib/mongodb
mkdir -p /var/lib/mongodb
chown -R mongodb.mongodb /var/lib/mongodb

# Re-apply original mongo server conf file
cat << 'EOF'  > /etc/mongod.conf
# mongod.conf

# for documentation of all options, see:
#   http://docs.mongodb.org/manual/reference/configuration-options/

# Where and how to store data.
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1


# how the process runs
processManagement:
  timeZoneInfo: /usr/share/zoneinfo

#security:

#operationProfiling:

#replication:

#sharding:

## Enterprise-Only Options:

#auditLog:

#snmp:
EOF

service mongod start



# Re-apply Promethium configuration 
. /usr/share/debconf/confmodule
   
# Fetching configuration from debconf
db_get promethium-mongodb/installType
CONFTYPE=$RET
        
db_get promethium-mongodb/adminPwd
ADMINPWD=$RET
        
db_get promethium-mongodb/promethiumAccountPwd
PROMETHIUMACCOUNTPWD=$RET

THISHOSTNAME=$(hostname -f)
/usr/share/promethium-mongodb/scripts/configure.sh -t "Standalone"  -n "$THISHOSTNAME" -a "$ADMINPWD" -p "$PROMETHIUMACCOUNTPWD"



## Promethium Re-init 
# Set first data set
echo -e "Loading now all primary objects on mongoDb, as collections, indexes, admin0@global.virt account... "
CMD="/usr/share/promethium-p3/bin/python3 /usr/share/promethium/scripts/mapping/_nosql/setPrimaryData.py"
eval $CMD

# Reset admin0@global.virt password
CMD="/usr/share/promethium-p3/bin/python3 /usr/share/promethium/scripts/mapping/_nosql/reinitAdmin0Password.py"
eval $CMD
